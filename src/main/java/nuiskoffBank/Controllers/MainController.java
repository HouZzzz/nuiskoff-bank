package nuiskoffBank.Controllers;

import nuiskoffBank.Entities.User;
import nuiskoffBank.Services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class MainController {
    @Autowired
    private UserService userService;
    private Logger LOG = LoggerFactory.getLogger(MainController.class);
    @GetMapping("/")
    public String mainPage(Principal principal) {
        return "main-page";
    }
    @GetMapping("/login")
    public String loginPage() {
        return "login";
    }

    @GetMapping("/registration")
    public String registrationPage() {
        return "registration";
    }

    @PostMapping("/registration")
    public String registerUser(@Valid @ModelAttribute User user, BindingResult result,Model model) {
        if (result.hasErrors()) {
            LOG.debug("validation failed with errors: " + result.getAllErrors().stream()
                    .map(e -> e.getDefaultMessage())
                    .collect(Collectors.toList()));

            for (FieldError err : result.getFieldErrors()) {
                model.addAttribute(err.getField() + "Err",err.getDefaultMessage());
            }
            return "registration";
        }
        userService.registerUser(user);
        return "redirect:/login";
    }

    @GetMapping("/admin")
    public String adminDesc(Model model, Principal principal) {
        User adminAccount = userService.getUserByUsername(principal.getName());
        model.addAttribute("name",adminAccount.getName());
        return "adminPage";
    }
}
