package nuiskoffBank.Controllers;

import nuiskoffBank.Entities.Terminal;
import nuiskoffBank.Entities.Transaction;
import nuiskoffBank.Entities.User;
import nuiskoffBank.Entities.Wrappers.ExceptionWrappers.ApiReturnableObject;
import nuiskoffBank.Entities.Wrappers.ExceptionWrappers.ExceptionWrapper;
import nuiskoffBank.Entities.Wrappers.ExceptionWrappers.SuccessfullyTransaction;
import nuiskoffBank.Entities.Wrappers.TerminalPreTransaction;
import nuiskoffBank.Exceptions.TransactionExceptions.WrongCardDataException;
import nuiskoffBank.Services.TerminalService;
import nuiskoffBank.Services.TransactionService;
import nuiskoffBank.Services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/nuiskoffBank/api")
public class RestTerminalController {
    @Autowired
    private TerminalService terminalService;

    @Autowired
    private UserService userService;

    @Autowired
    private TransactionService transactionService;

    private Logger LOG = LoggerFactory.getLogger(RestTerminalController.class);


    @GetMapping("/terminals/{token}")
    public Object terminalByToken(@PathVariable String token) {
        LOG.debug("api: getting terminal by token " + token);
        Terminal terminal = terminalService.getTerminalByToken(token);
        if (terminal == null) {
            return new ExceptionWrapper(
                    "NullPointerException",
                    "Cant find terminal with token " + token
            );
        } else {
            return terminal;
        }
    }

    @PostMapping("/terminals/transactions")
    public Object createTransaction(@RequestBody TerminalPreTransaction preTransaction) {
        Terminal terminal = terminalService.getTerminalByToken(
                preTransaction.getTerminalToken());
        if (terminal == null) {
            LOG.debug("terminal with token " + terminal.getToken() + " did not found");
            return new ExceptionWrapper(
                    "NullPointerException",
                    "cant find terminal with token " + preTransaction.getTerminalToken()
            );
        }

        if (!terminal.isActive()) {
            LOG.debug("terminal " + preTransaction.getTerminalToken() + " is not active");
            return new ExceptionWrapper(
                    "TerminalIsNotActiveException",
                    "terminal is not active, transaction canceled"
            );
        }
        LOG.info("api: creating transaction by terminal pre transaction " + preTransaction);
        Transaction transaction = new Transaction();

        User sender = null;
        try {
            sender = userService.getUserByCardNumber(preTransaction.getSenderCardNumber());
        } catch (WrongCardDataException e) {
            LOG.debug("wrong card data");
            return new ExceptionWrapper(
                    e.getClass().getSimpleName(),
                    e.getMessage()
            );
        }
        if ( // wrong data
                sender == null ||
                !sender.getTerm().equals(preTransaction.getSenderTerm()) ||
                Integer.parseInt(sender.getCvc()) != preTransaction.getSenderCvc()
        ) {
            LOG.debug("wrong card data");
            return new ExceptionWrapper(
                    "WrongCardDataException",
                    "Wrong card number/term/cvc"
            );
        }
        if (sender.getBalance() < preTransaction.getAmount()) {
            return new ExceptionWrapper(
                    "enoughBalanceException",
                    "the sender does not have enough funds"
            );
        }

        transaction.setSender(sender.getUsername());
        transaction.setGetterTerminalToken(preTransaction.getTerminalToken());
        transaction.setAmount(preTransaction.getAmount());
        transaction.setDate(new Timestamp(new Date().getTime()));
        LOG.debug("created transaction entity for terminal: " + transaction);

        transactionService.createTransaction(transaction);

        return new SuccessfullyTransaction();
    }
    @GetMapping("/terminals/transactions/{token}")
    public Object getTransactionsByTerminalToken(@PathVariable String token) {
        LOG.debug("api: getting transactions by terminal token " + token);
        List<Transaction> transactions = transactionService.getTransactionsByTerminalToken(token);
        if (transactions.isEmpty()) {
            return new ExceptionWrapper(
                    "NullPointerException",
                    "wrong terminal token"
            );
        }

        return transactions;
    }
}
