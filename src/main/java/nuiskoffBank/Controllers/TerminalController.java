package nuiskoffBank.Controllers;

import nuiskoffBank.Entities.Terminal;
import nuiskoffBank.Entities.Wrappers.MessageWrapper;
import nuiskoffBank.Services.TerminalService;
import nuiskoffBank.Services.UserService;
import nuiskoffBank.TelegramBot.MarkupMaker;
import nuiskoffBank.TelegramBot.TgBot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.security.Principal;
import java.util.List;

@Controller
public class TerminalController {
    @Autowired
    private TerminalService terminalService;

    @Autowired
    private UserService userService;

    @Autowired
    private TgBot bot;

    private static Logger LOG = LoggerFactory.getLogger(TerminalController.class);

    @GetMapping("/confirm-terminal/{token}")
    public String confirmTerminal(@PathVariable String token, Model model) {
        LOG.debug("confirming terminal " + token);
        Terminal terminal = terminalService.getTerminalByToken(token);
        if (terminal.isActive()) {
            LOG.debug("terminal was already active");
            model.addAttribute("terminal",terminal);
            return "terminal-confirmed";
        }

        terminal = terminalService.confirmTerminal(token);

        LOG.debug("adding role to user " + terminal.getOwner());
        userService.addRoleToUser(terminal.getOwner(),"ROLE_TERMINAL_OWNER");

        try {
            bot.sendMessage(
                    new MessageWrapper(MarkupMaker.markupMaker("Мои терминалы","myTerminals"),"Терминал *" + terminal.getName() + "* подтвержден!\nТокен терминала: `" + token + "`"),
                    terminal.getOwnerID()
            );
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }

        model.addAttribute("terminal",terminal);
        return "terminal-confirmed";
    }

    @GetMapping("/terminals")
    public String terminals(Principal principal,Model model) {
        List<Terminal> terminals = terminalService.getTerminalsByUsername(principal.getName());
        model.addAttribute("terminals",terminals);
        return "terminals";
    }
}
