package nuiskoffBank.Controllers;

import nuiskoffBank.Entities.Wrappers.PreTransaction;
import nuiskoffBank.Services.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.stream.Collectors;

@Controller
public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    private Logger LOG = LoggerFactory.getLogger(TransactionController.class);

    @GetMapping("/transaction")
    public String transactions(Principal principal, Model model) {
        LOG.debug("get /transactions with principal name " + principal.getName());
        model.addAttribute("transactions",transactionService.getTransactionsByUsername(principal.getName()));
        return "my-transactions";
    }

    // todo не работает валидация ???
    @PostMapping("/transaction")
    public String createTransaction(@Valid @ModelAttribute PreTransaction preTransaction, BindingResult result, Principal principal, Model model) {
        LOG.debug("post /transaction with pre transaction object " + preTransaction + " and principal name " + principal.getName());
        if (result.hasErrors()) {
            LOG.debug("validation failed with errors: " + result.getAllErrors().stream()
                    .map(e -> e.getDefaultMessage())
                    .collect(Collectors.toList()));

            for (FieldError err : result.getFieldErrors()) {
                model.addAttribute(err.getField() + "Err", err.getDefaultMessage());
            }
            model.addAttribute("transactions",transactionService.getTransactionsByUsername(principal.getName()));
            return "my-transactions";
        }

        transactionService.createTransaction(principal.getName(),preTransaction);
        return "redirect:/transaction";
    }
}
