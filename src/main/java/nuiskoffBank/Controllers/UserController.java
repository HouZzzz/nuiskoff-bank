package nuiskoffBank.Controllers;

import nuiskoffBank.Entities.Wrappers.PreTransaction;
import nuiskoffBank.Services.TransactionService;
import nuiskoffBank.Services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    private Logger LOG = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/me")
    public String mePage(Principal principal, Model model){
        LOG.debug("get /me with principal name " + principal.getName());
        model.addAttribute("user",userService.modifyUserForProfilePage(
                userService.getUserByUsername(principal.getName())));
        return "me";
    }
}
