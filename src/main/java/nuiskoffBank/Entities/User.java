package nuiskoffBank.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Table(name = "users")
@Entity
public class User {
    @Id
    @Column(name = "username")
    @NotBlank(message = "не должно быть пустым")
    @Size(min = 3, max = 20, message = "логин должен содержать от 3 до 20 символов")
    private String username;

    @Column(name = "password")
    @Size(min = 5, message = " пароль должен содержать от 5 символов")
    private String password;
    @Column(name = "name")
    @Size(min = 2, message = "Имя должно содержать минимум 2 символа")
    private String name;
    @Column(name = "surname")
    @Size(min = 2, message = "Фамилия должна содержать минимум 2 символа")
    private String surname;
    @Column(name = "email")
    @Pattern(regexp = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$",message = "Неверно ведена почта")
    private String email;
    @Column(name = "card_number")
    private String cardNumber;
    @Column(name = "card_term")
    private String term;
    @Column(name = "card_cvc")
    private String cvc;

    @Column(name = "card_balance")
    private int balance;

//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "id")
//    private List<Transaction> transactions;
    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", term='" + term + '\'' +
                ", cvc='" + cvc + '\'' +
                ", balance=" + balance +
                '}';
    }
}
