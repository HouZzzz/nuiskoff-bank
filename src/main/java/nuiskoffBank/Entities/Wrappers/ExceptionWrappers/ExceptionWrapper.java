package nuiskoffBank.Entities.Wrappers.ExceptionWrappers;

public class ExceptionWrapper extends ApiReturnableObject {
    private String excepion;
    private String reason;

    public ExceptionWrapper(String excepion, String reason) {
        this.excepion = excepion;
        this.reason = reason;
    }

    public ExceptionWrapper() {
    }

    public String getExcepion() {
        return excepion;
    }

    public void setExcepion(String excepion) {
        this.excepion = excepion;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
