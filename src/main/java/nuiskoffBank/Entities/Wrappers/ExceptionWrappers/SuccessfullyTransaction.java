package nuiskoffBank.Entities.Wrappers.ExceptionWrappers;

public class SuccessfullyTransaction extends ApiReturnableObject {
    String status = "successfully";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
