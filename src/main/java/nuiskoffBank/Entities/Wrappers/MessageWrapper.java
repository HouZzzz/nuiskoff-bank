package nuiskoffBank.Entities.Wrappers;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;

import java.util.List;

public class MessageWrapper {
    private InlineKeyboardMarkup markups;
    private String message;
    private Long chatID = 0L;
    private boolean isPoll = false;
    private List<String> options;

    public MessageWrapper(InlineKeyboardMarkup markups, String message) {
        this.markups = markups;
        this.message = message;
    }

    public MessageWrapper(InlineKeyboardMarkup markups, String message,Long chatID) {
        this.markups = markups;
        this.message = message;
        this.chatID = chatID;
    }

    public MessageWrapper(InlineKeyboardMarkup markups, String message, boolean isPoll,List<String> options) {
        this.markups = markups;
        this.message = message;
        this.isPoll = isPoll;
        this.options = options;
    }

    @Override
    public String toString() {
        return "MessageWrapper{" +
                "markups=" + markups +
                ", message='" + message + '\'' +
                '}';
    }

    public InlineKeyboardMarkup getMarkups() {
        return markups;
    }

    public void setMarkups(InlineKeyboardMarkup markups) {
        this.markups = markups;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getChatID() {
        return chatID;
    }

    public void setChatID(Long chatID) {
        this.chatID = chatID;
    }

    public boolean isPoll() {
        return isPoll;
    }

    public void setPoll(boolean poll) {
        isPoll = poll;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }
}
