package nuiskoffBank.Entities.Wrappers;

import javax.validation.constraints.*;

public class PreTransaction {

    @Pattern(regexp="\\d{16}",message = "Неверный номер карты")
    private String getterCardNumber;

    @Min(value = 10,message = "Минимум 10 рублей")
    @Max(value = 1_000_000,message = "Максимум 1 миллион рублей")
    private int amount;
    private String message;

    public PreTransaction(String getterCardNumber, int amount, String message) {
        this.getterCardNumber = getterCardNumber;
        this.amount = amount;
        this.message = message;
    }

    public PreTransaction() {
    }

    public String getGetterCardNumber() {
        return getterCardNumber;
    }

    public void setGetterCardNumber(String getterCardNumber) {
        this.getterCardNumber = getterCardNumber;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "PreTransaction{" +
                "getterCardNumber='" + getterCardNumber + '\'' +
                ", amount=" + amount +
                ", message='" + message + '\'' +
                '}';
    }
}
