package nuiskoffBank.Entities.Wrappers;

public class TerminalPreTransaction {
    private String senderCardNumber;
    private String senderTerm;
    private int senderCvc;
    private String terminalToken;
    private int amount;

    public TerminalPreTransaction(String senderCardNumber, String senderTerm, int senderCvc, String terminalToken, int amount) {
        this.senderCardNumber = senderCardNumber;
        this.senderTerm = senderTerm;
        this.senderCvc = senderCvc;
        this.terminalToken = terminalToken;
        this.amount = amount;
    }

    public TerminalPreTransaction() {
    }

    public String getSenderCardNumber() {
        return senderCardNumber;
    }

    public void setSenderCardNumber(String senderCardNumber) {
        this.senderCardNumber = senderCardNumber;
    }

    public String getTerminalToken() {
        return terminalToken;
    }

    public void setTerminalToken(String terminalToken) {
        this.terminalToken = terminalToken;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getSenderTerm() {
        return senderTerm;
    }

    public void setSenderTerm(String senderTerm) {
        this.senderTerm = senderTerm;
    }

    public int getSenderCvc() {
        return senderCvc;
    }

    public void setSenderCvc(int senderCvc) {
        this.senderCvc = senderCvc;
    }

    @Override
    public String toString() {
        return "TerminalPreTransaction{" +
                "senderCardNumber='" + senderCardNumber + '\'' +
                ", senderTerm='" + senderTerm + '\'' +
                ", senderCvc=" + senderCvc +
                ", terminalToken='" + terminalToken + '\'' +
                ", amount=" + amount +
                '}';
    }
}
