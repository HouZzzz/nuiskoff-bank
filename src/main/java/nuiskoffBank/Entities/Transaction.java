package nuiskoffBank.Entities;

import nuiskoffBank.Entities.Wrappers.PreTransaction;
import nuiskoffBank.Entities.Wrappers.TerminalPreTransaction;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Random;

@Table(name = "transactions")
@Entity
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "sender_username")
    private String sender;

    @Column(name = "getter_username")
    private String getter;

    @Column(name = "getter_terminal_token")
    private String getterTerminalToken;

    @Column(name = "amount")
    private int amount;

    @Column(name = "transaction_date")
    private Timestamp date;

    @Column(name = "confirm_code")
    private int confirmCode;

    @Column(name = "confirmed")
    private boolean confirmed;

    @Column(name = "message")
    private String message;

    public Transaction() {
    }

    public Transaction(PreTransaction preTransaction) {
        confirmed = true;
        confirmCode = 1111;
    }

    public Transaction(TerminalPreTransaction preTransaction) {
        sender = preTransaction.getSenderCardNumber();
        getterTerminalToken = preTransaction.getTerminalToken();
        amount = preTransaction.getAmount();

        date = new Timestamp(new Date().getTime());
        confirmCode = 1234; //todo сделать генератор кода подтверждения
        confirmed = true; //todo поменять на false и сделать подтверждение через код который отправляется на почту
    }

    public Transaction(String sender, String getter, String getterTerminalToken, int amount, Timestamp date, int confirmCode, boolean confirmed, String message) {
        this.sender = sender;
        this.getter = getter;
        this.getterTerminalToken = getterTerminalToken;
        this.amount = amount;
        this.date = date;
        this.confirmCode = confirmCode;
        this.confirmed = confirmed;
        this.message = message;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", sender='" + sender + '\'' +
                ", getter='" + getter + '\'' +
                ", getterTerminalToken='" + getterTerminalToken + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", confirmCode=" + confirmCode +
                ", confirmed=" + confirmed +
                ", message='" + message + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getGetter() {
        return getter;
    }

    public void setGetter(String getter) {
        this.getter = getter;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getConfirmCode() {
        return confirmCode;
    }

    public void setConfirmCode(int confirmCode) {
        this.confirmCode = confirmCode;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getGetterTerminalToken() {
        return getterTerminalToken;
    }

    public void setGetterTerminalToken(String getterTerminalToken) {
        this.getterTerminalToken = getterTerminalToken;
    }
}
