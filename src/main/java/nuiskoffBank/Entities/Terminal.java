package nuiskoffBank.Entities;

import javax.persistence.*;

@Entity
@Table(name = "terminals")
public class Terminal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "token")
    private String token;

    @Column(name = "balance")
    private int balance;

    @Column(name = "category")
    private String category;

    @Column(name = "owner")
    private String owner;

    @Column(name = "owner_telegram_login")
    private Long ownerID;

    @Column(name = "active")
    private boolean active;

    public Terminal() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(Long ownerID) {
        this.ownerID = ownerID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Terminal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", token='" + token + '\'' +
                ", balance=" + balance +
                ", category='" + category + '\'' +
                ", owner='" + owner + '\'' +
                ", ownerID=" + ownerID +
                ", active=" + active +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Terminal terminal = (Terminal) o;

        if (id != terminal.id) return false;
        if (balance != terminal.balance) return false;
        if (active != terminal.active) return false;
        if (name != null ? !name.equals(terminal.name) : terminal.name != null) return false;
        if (token != null ? !token.equals(terminal.token) : terminal.token != null) return false;
        if (category != null ? !category.equals(terminal.category) : terminal.category != null) return false;
        if (owner != null ? !owner.equals(terminal.owner) : terminal.owner != null) return false;
        return ownerID != null ? ownerID.equals(terminal.ownerID) : terminal.ownerID == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        result = 31 * result + balance;
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (ownerID != null ? ownerID.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }
}
