package nuiskoffBank.Entities;

import javax.persistence.*;

@Entity
@Table(name = "tech_support_feedbacks")
public class TechSupportFeedback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(name = "feedback_from")
    private String from;

    @Column(name = "feedback_to")
    private String to;

    @Column
    private int value;

    public TechSupportFeedback(String from, int value) {
        this.from = from;
        this.value = value;
    }

    public TechSupportFeedback() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TechSupportFeedback{" +
                "id=" + id +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", value=" + value +
                '}';
    }
}
