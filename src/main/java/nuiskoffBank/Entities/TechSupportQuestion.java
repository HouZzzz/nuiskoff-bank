package nuiskoffBank.Entities;

import nuiskoffBank.Enums.QuestionStatuses;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "tech_support_question")
public class TechSupportQuestion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column
    private String problem;

    @Column(name = "creator_telegram_id")
    private String creatorTelegramID;

    @Column
    private boolean closed;

    @Column
    private String status = "free";

    @Column(name = "creation_date")
    private Timestamp creationDate;

    @Column(name = "taken_by")
    private String takenBy;


    public TechSupportQuestion() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getCreatorTelegramID() {
        return creatorTelegramID;
    }

    public void setCreatorTelegramID(String creatorTelegramID) {
        this.creatorTelegramID = creatorTelegramID;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(QuestionStatuses status) {
        this.status = status.getStatus();
    }

    public String getTakenBy() {
        return takenBy;
    }

    public void setTakenBy(String takenBy) {
        this.takenBy = takenBy;
    }

    @Override
    public String toString() {
        return "TechSupportQuestion{" +
                "id=" + id +
                ", problem='" + problem + '\'' +
                ", creatorTelegramID='" + creatorTelegramID + '\'' +
                ", closed=" + closed +
                ", status='" + status + '\'' +
                ", creationDate=" + creationDate +
                ", takenBy='" + takenBy + '\'' +
                '}';
    }
}
