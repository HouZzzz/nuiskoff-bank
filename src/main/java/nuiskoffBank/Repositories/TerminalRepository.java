package nuiskoffBank.Repositories;

import nuiskoffBank.Entities.Terminal;

import java.util.List;

public interface TerminalRepository {
    public List<Terminal> getAllTerminals();
    public Terminal getTerminalByToken(String token);
    public void createTerminal(Terminal terminal);
    public List<Terminal> getTerminalByOwnerTelegramID(Long id);
    public Terminal confirmTerminal(String token);
    public List<Terminal> getTerminalsByUsername(String username);
    public Terminal getTerminalByName(String name);
}
