package nuiskoffBank.Repositories;

import nuiskoffBank.Entities.TechSupportFeedback;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FeedbackRepositoryImpl implements FeedbackRepository {
    @Autowired
    private SessionFactory sessionFactory;

    private static Logger LOG = LoggerFactory.getLogger(FeedbackRepository.class);

    @Override
    public void saveFeedback(TechSupportFeedback feedback) {
        sessionFactory.getCurrentSession().persist(feedback);
        LOG.debug("feedback " + feedback + " saved");
    }

    @Override
    public List<TechSupportFeedback> getFeedbacksAboutEmployee(String employeeUsername) {
        Session session = sessionFactory.getCurrentSession();
        LOG.debug("searching feedbacks about @" + employeeUsername);

        // fixme транзакция откатывается
        List<TechSupportFeedback> feedbacks = session.createQuery("from TechSupportFeedback where to = '%s'".formatted(employeeUsername), TechSupportFeedback.class)
                .getResultList();

        LOG.debug("found list of feedbacks: " + feedbacks);
        return feedbacks;
    }
}
