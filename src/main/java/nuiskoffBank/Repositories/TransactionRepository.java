package nuiskoffBank.Repositories;

import nuiskoffBank.Entities.Transaction;
import nuiskoffBank.Entities.Wrappers.PreTransaction;

import java.util.List;

public interface TransactionRepository {
    public List<Transaction> getTransactionsByUsername(String username);
    public void createTransaction(String principal, PreTransaction preTransaction);
    public void createTransaction(Transaction transaction);
    public List<Transaction> getTransactionsByTerminalToken(String token);
}
