package nuiskoffBank.Repositories;

import nuiskoffBank.Entities.Authority;
import nuiskoffBank.Entities.User;

import java.util.List;

public interface UserRepository {
    public void registerUser(User user);
    public User getUserByUsername(String username);
    public User getUserByCardNumber(String cardNumber);
    public void saveUser(User user);
    public void addRoleToUser(String username,String role);
    public List<Authority> getUserAuthorities(String username);
}
