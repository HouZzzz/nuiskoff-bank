package nuiskoffBank.Repositories;

import nuiskoffBank.Entities.Terminal;
import nuiskoffBank.Entities.Wrappers.MessageWrapper;
import nuiskoffBank.TelegramBot.TgBot;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Repository;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

@Repository
public class TerminalRepositoryImpl implements TerminalRepository {
    @Autowired
    private SessionFactory sessionFactory;

    private Logger LOG = LoggerFactory.getLogger(TerminalRepositoryImpl.class);

    @Override
    public List<Terminal> getAllTerminals() {
        List<Terminal> terminals = sessionFactory
                .getCurrentSession()
                .createQuery("from Terminal ", Terminal.class)
                .getResultList();
        LOG.debug("found terminals: " + terminals);
        return terminals;
    }

    @Override
    public Terminal getTerminalByToken(String token) {
        List<Terminal> list = sessionFactory
                .getCurrentSession()
                .createQuery("from Terminal where token = '%s'".formatted(token))
                .getResultList();
        LOG.debug("found terminal by token " + token + ": " + list);
        return list.size() == 0 ? null : list.get(0);
    }

    @Override
    public void createTerminal(Terminal terminal) {
        LOG.debug("saving terminal " + terminal);
        sessionFactory.getCurrentSession()
            .persist(terminal);
        LOG.debug("terminal saved successfully");
    }

    @Override
    public List<Terminal> getTerminalByOwnerTelegramID(Long id) {
        Session session = sessionFactory.getCurrentSession();
        List<Terminal> terminals = session.createQuery("from Terminal where ownerID = " + id, Terminal.class).getResultList();
        LOG.debug("found terminals with owner telegram id " + id + ": " + terminals);
        return terminals.size() == 0 ? null : terminals;
    }

    @Override
    public Terminal confirmTerminal(String token) {
        Terminal terminal = getTerminalByToken(token);
        if (terminal != null) {
            terminal.setActive(true);
            sessionFactory
                    .getCurrentSession()
                    .update(terminal);
        }
        return terminal;
    }

    @Override
    public List<Terminal> getTerminalsByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        List<Terminal> terminals = session.createQuery("from Terminal where owner = '%s' ".formatted(username), Terminal.class)
                .getResultList();

        return terminals;
    }

    @Override
    public Terminal getTerminalByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        List<Terminal> terminals = session.createQuery("from Terminal where name = '%s' ".formatted(name),Terminal.class).getResultList();

        LOG.debug("found list of terminal with name " + name + ": " + terminals);
        return terminals.size() == 0 ? null : terminals.get(0);
    }
}
