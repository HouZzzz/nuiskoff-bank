package nuiskoffBank.Repositories;

import nuiskoffBank.Controllers.UserController;
import nuiskoffBank.Entities.Terminal;
import nuiskoffBank.Entities.Transaction;
import nuiskoffBank.Entities.User;
import nuiskoffBank.Entities.Wrappers.PreTransaction;
import nuiskoffBank.Exceptions.TransactionExceptions.WrongCardDataException;
import nuiskoffBank.Services.TerminalService;
import nuiskoffBank.Services.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Repository
public class TransactionRepositoryImpl implements TransactionRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private UserService userService;
    @Autowired
    private TerminalService terminalService;
    private Logger LOG = LoggerFactory.getLogger(TransactionRepositoryImpl.class);
    @Override
    public List<Transaction> getTransactionsByUsername(String username) {
        LOG.debug("getting list of transactions with username " + username);
        Session session = sessionFactory.getCurrentSession();
        List<Transaction> transactions = session
                .createQuery("from Transaction where getter = '%s' or sender = '%s' order by id desc".formatted(username, username))
                .getResultList();

        LOG.debug("transactions with username " + username + " found in db: " + transactions.toString().substring(0,200) + "...");
        return transactions;
    }

    @Override
    public void createTransaction(String principal, PreTransaction preTransaction) {
        User getter = null;
        try {
            getter = userService.getUserByCardNumber(preTransaction.getGetterCardNumber());
        } catch (WrongCardDataException e) {
            e.printStackTrace();
            return;
        }
        User sender = userService.getUserByUsername(principal);

        if (sender.getBalance() < preTransaction.getAmount()) {
            LOG.debug("the user %s does not have enough funds (%d)".formatted(principal,preTransaction.getAmount()));
            return;
        }
        LOG.info("creating transaction by pre transactional " + preTransaction + " and principal name " + principal);

        Transaction transaction = new Transaction();
        transaction.setAmount(preTransaction.getAmount());
        transaction.setGetter(getter.getUsername());
        transaction.setSender(sender.getUsername());
        Random random = new Random();
        int confirmCode = random.nextInt(10000);
        while (confirmCode < 1000) {
            confirmCode = confirmCode * 10 + random.nextInt(10);
        }
        transaction.setConfirmCode(confirmCode);
        transaction.setConfirmed(true); //todo сменить на false по умолчанию и сделать систему подтверждения платежей ПО ТЕРМИНАЛАМ, не переводы
        transaction.setMessage(preTransaction.getMessage());
        transaction.setDate(new Timestamp(new Date().getTime()));

        LOG.debug("created transaction object " + transaction);

        Session session = sessionFactory.getCurrentSession();
        sender.setBalance(sender.getBalance() - preTransaction.getAmount());
        getter.setBalance(getter.getBalance() + preTransaction.getAmount());

        userService.saveUser(sender);
        userService.saveUser(getter);
        LOG.debug("users data updated");
        session.persist(transaction);
        LOG.info("transaction with id " + transaction.getId() + " completed successfully");
    }

    @Override
    public void createTransaction(Transaction transaction) {
        Session session = sessionFactory.getCurrentSession();
        User sender = userService.getUserByUsername(transaction.getSender());
        Terminal getter = terminalService.getTerminalByToken(transaction.getGetterTerminalToken());

        sender.setBalance(sender.getBalance() - transaction.getAmount());
        getter.setBalance(getter.getBalance() + transaction.getAmount());

        session.update(sender);
        session.update(getter);

        session.persist(transaction);
        LOG.info("successfully created transaction " + transaction);
    }

    @Override
    public List<Transaction> getTransactionsByTerminalToken(String token) {
        List<Transaction> transactions = sessionFactory.getCurrentSession()
                .createQuery("from Transaction where getterTerminalToken = '%s'".formatted(token), Transaction.class)
                .getResultList();
        LOG.debug("found transactions by terminal token " + token + ": " + transactions);
        return transactions;
    }
}
