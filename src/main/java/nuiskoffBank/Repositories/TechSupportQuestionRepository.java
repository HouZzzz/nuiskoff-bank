package nuiskoffBank.Repositories;

import nuiskoffBank.Entities.TechSupportQuestion;

import java.util.List;

public interface TechSupportQuestionRepository {
    public TechSupportQuestion createQuestion(TechSupportQuestion question);
    public List<TechSupportQuestion> getOpenQuestions();
    public TechSupportQuestion getQuestionById(int id);
    public void closeQuestion(Long chatID);
    public TechSupportQuestion getLastUserQuestion(String userID);
}
