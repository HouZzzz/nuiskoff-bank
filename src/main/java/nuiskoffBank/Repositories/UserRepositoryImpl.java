package nuiskoffBank.Repositories;

import nuiskoffBank.Controllers.MainController;
import nuiskoffBank.Entities.Authority;
import nuiskoffBank.Entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserRepositoryImpl implements UserRepository {
    @Autowired
    private SessionFactory sessionFactory;
    private Logger LOG = LoggerFactory.getLogger(UserRepositoryImpl.class);

    @Override
    public void registerUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(user);
        session.persist(new Authority(user.getUsername(),"ROLE_USER"));
        LOG.debug("registered new user with username " + user.getUsername());
    }

    @Override
    public User getUserByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        List<User> resultList = session.createQuery("from User where username = '%s' ".formatted(username), User.class).getResultList();
        LOG.debug("found list of users with username " + username + ": " + resultList);
        return resultList.size() == 0 ? null : resultList.get(0);
    }

    @Override
    public User getUserByCardNumber(String cardNumber) {
        Session session = sessionFactory.getCurrentSession();
        List<User> users = session.createQuery("from User where cardNumber = '%s'".formatted(cardNumber), User.class).getResultList();
        LOG.debug("found user with card number " + cardNumber + ": " + users);
        return users.size() == 0 ? null : users.get(0);
    }

    @Override
    public void saveUser(User user) {
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }

    @Override
    public void addRoleToUser(String username,String role) {
        Session session = sessionFactory.getCurrentSession();
        List<Authority> authorities = session.createQuery("from Authority where username = '%s' ".formatted(username), Authority.class)
                .getResultList();
        LOG.debug("found authorities of user " + username + ": " + authorities);
        boolean hasRole = authorities.stream()
                .filter(a -> a.getAuthority().equals(role))
                .collect(Collectors.toList())
                .size() >= 1;
        if (hasRole) {
            LOG.debug(username + " already have " + role);
            return;
        }

        LOG.debug("adding role " + role + " to user " + username);
        session.persist(new Authority(username,role));
        LOG.debug("added role " + role + " to user " + username);
    }

    @Override
    public List<Authority> getUserAuthorities(String username) {
        Session session = sessionFactory.getCurrentSession();

        List<Authority> authorities = session.createQuery("from Authority where username = '%s' ".formatted(username), Authority.class)
                .getResultList();
        LOG.debug("found authorities of user " + username + ": " + authorities);
        return authorities;
    }
}
