package nuiskoffBank.Repositories;

import nuiskoffBank.Entities.TechSupportQuestion;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class TechSupportQuestionRepositoryImpl implements TechSupportQuestionRepository {
    @Autowired
    private SessionFactory sessionFactory;

    private static Logger LOG = LoggerFactory.getLogger(TechSupportQuestionRepository.class);

    @Override
    public TechSupportQuestion createQuestion(TechSupportQuestion question) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(question);

        LOG.debug("question saved");
        return question;
    }

    @Override
    public List<TechSupportQuestion> getOpenQuestions() {
        List<TechSupportQuestion> questions = sessionFactory
                .getCurrentSession()
                .createQuery("from TechSupportQuestion where closed = false and takenBy = null", TechSupportQuestion.class)
                .getResultList();

        LOG.debug("found list of open questions: " + questions);
        return questions;
    }

    @Override
    public TechSupportQuestion getQuestionById(int id) {
        LOG.debug("getting question by id " + id);
        Session session = sessionFactory.getCurrentSession();

        TechSupportQuestion question = session.get(TechSupportQuestion.class, id);
        LOG.debug("found question: " + question);
        return question;
    }

    @Override
    public void closeQuestion(Long chatID) {
        Session session = sessionFactory.getCurrentSession();
        List<TechSupportQuestion> questions = session
                .createQuery("from TechSupportQuestion where creatorTelegramID = '%s' and closed = false".formatted(String.valueOf(chatID)), TechSupportQuestion.class)
                .getResultList();

        LOG.debug("closing list of questions: " + questions);
        questions.forEach(question -> {
            question.setClosed(true);
            session.update(question);
        });
    }

    @Override
    public TechSupportQuestion getLastUserQuestion(String userID) {
        Session session = sessionFactory.getCurrentSession();

        LOG.debug("searching questions by userID " + userID);
        List<TechSupportQuestion> userQuestions = session.createQuery("from TechSupportQuestion where creatorTelegramID = '%s' order by id desc".formatted(userID), TechSupportQuestion.class)
                .getResultList();
        LOG.debug("found list of questions: " + userQuestions);
        return userQuestions.get(0); // last

    }
}
