package nuiskoffBank.Repositories;

import nuiskoffBank.Entities.TechSupportFeedback;

import java.util.List;

public interface FeedbackRepository {
    public void saveFeedback(TechSupportFeedback feedback);
    public List<TechSupportFeedback> getFeedbacksAboutEmployee(String employeeUsername);
}
