package nuiskoffBank.Enums;

public enum QuestionStatuses {
    FREE("free"),
    TAKEN("taken");

    String status;
    QuestionStatuses(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
