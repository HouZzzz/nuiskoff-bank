package nuiskoffBank.Enums;

public enum TerminalCategories {
    MARKET("Магазин"),
    INTERNET_MARKET("Интернет-магазин"),
    FAST_FOOD("Фастфуд"),
    TRANSPORT("Транспорт"),
    OTHER("Различные-товары");




    TerminalCategories(String category) {
        this.category = category;
    }
    String category;

    public String getCategory() {
        return category;
    }
}
