package nuiskoffBank.Services;

import nuiskoffBank.Entities.TechSupportQuestion;
import nuiskoffBank.Enums.QuestionStatuses;
import nuiskoffBank.Repositories.TechSupportQuestionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class TechSupportQuestionServiceImpl implements TechSupportQuestionService {
    @Autowired
    private TechSupportQuestionRepository tsqRepo;

    private static Logger LOG = LoggerFactory.getLogger(TechSupportQuestionService.class);
    @Override
    @Transactional
    public TechSupportQuestion createQuestion(String problem, Long creatorTelegramID) {
        TechSupportQuestion questionObject = new TechSupportQuestion();

        questionObject.setCreationDate(new Timestamp(new Date().getTime()));
        questionObject.setCreatorTelegramID(creatorTelegramID.toString());
        questionObject.setProblem(problem);

        LOG.debug("created tech support question: " + questionObject);

        return tsqRepo.createQuestion(questionObject);
    }

    @Override
    @Transactional
    public List<TechSupportQuestion> getOpenQuestions() {
        return tsqRepo.getOpenQuestions();
    }

    @Override
    @Transactional
    public TechSupportQuestion getQuestionById(int id) {
        return tsqRepo.getQuestionById(id);
    }

    @Override
    @Transactional
    public void saveQuestion(TechSupportQuestion question) {
        tsqRepo.createQuestion(question);
    }

    @Override
    @Transactional
    public TechSupportQuestion requestToAnswer(int questionID,String takenBy) {
        TechSupportQuestion question = tsqRepo.getQuestionById(questionID);

        if (question.isClosed() ||
            question.getStatus().equals(QuestionStatuses.TAKEN.getStatus())) {

            LOG.debug("failed to change status of #question" + questionID + ": question already taken by another employee or closed");
            return null;
        }

        question.setStatus(QuestionStatuses.TAKEN);
        question.setTakenBy(takenBy);
        LOG.debug("status of #question" + questionID + " changed to " + question.getStatus() + " by " + takenBy);
        tsqRepo.createQuestion(question);

        return question;
    }

    @Override
    @Transactional
    public void closeQuestion(Long chatID) {
        LOG.debug("request to close all question with creatorID " + chatID);
        tsqRepo.closeQuestion(chatID);
    }

    @Override
    @Transactional
    public TechSupportQuestion getLastUserQuestion(String userID) {
        return tsqRepo.getLastUserQuestion(userID);
    }
}
