package nuiskoffBank.Services;

import nuiskoffBank.Entities.Transaction;
import nuiskoffBank.Entities.User;
import nuiskoffBank.Entities.Wrappers.PreTransaction;
import nuiskoffBank.Repositories.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Random;

@Service
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    private TransactionRepository transactionRepo;

    @Autowired
    private UserService userService;

    private Logger LOG = LoggerFactory.getLogger(TransactionServiceImpl.class);

    @Override
    @Transactional
    public List<Transaction> getTransactionsByUsername(String username) {
        return transactionRepo.getTransactionsByUsername(username);
    }

    @Override
    @Transactional
    public void createTransaction(String principal, PreTransaction preTransaction) {
        preTransaction.setGetterCardNumber(preTransaction.getGetterCardNumber().replaceAll(" ",""));
        transactionRepo.createTransaction(principal,preTransaction);
    }

    @Override
    @Transactional
    public void createTransaction(Transaction transaction) {
        if (transaction.getGetter() == null && transaction.getGetterTerminalToken() != null) {
            //generate confirm code
            Random random = new Random();
            int code = 0;
            while (code < 9999) {
                code *= 10;
                code += 9 - random.nextInt(9);
            }
            // todo need to verify transaction by code
        }
        transactionRepo.createTransaction(transaction);
    }

    @Override
    @Transactional
    public List<Transaction> getTransactionsByTerminalToken(String token) {
        LOG.debug("finding transactions by terminal token: " + token);
        return transactionRepo.getTransactionsByTerminalToken(token);
    }
}
