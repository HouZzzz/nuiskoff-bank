package nuiskoffBank.Services;

import nuiskoffBank.Entities.Terminal;

import java.util.List;

public interface TerminalService {
    public List<Terminal> getAllTerminals();
    public Terminal getTerminalByToken(String token);
    public Terminal createTerminal(String owner,String category,String name, Long telegramID);
    public List<Terminal> getTerminalByOwnerTelegramID(Long id);
    public Terminal confirmTerminal(String token);
    public List<Terminal> getTerminalsByUsername(String username);

    public Terminal getTerminalByName(String name);
}
