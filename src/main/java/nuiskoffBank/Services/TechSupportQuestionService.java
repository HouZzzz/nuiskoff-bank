package nuiskoffBank.Services;

import nuiskoffBank.Entities.TechSupportQuestion;

import java.util.List;

public interface TechSupportQuestionService {
    public TechSupportQuestion createQuestion(String problem, Long creatorTelegramID);
    public List<TechSupportQuestion> getOpenQuestions();
    public TechSupportQuestion getQuestionById(int id);
    public void saveQuestion(TechSupportQuestion question);
    public TechSupportQuestion requestToAnswer(int questionID,String takenBy);
    public void closeQuestion(Long chatID);
    public TechSupportQuestion getLastUserQuestion(String userID);
}
