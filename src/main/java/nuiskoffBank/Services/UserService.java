package nuiskoffBank.Services;

import nuiskoffBank.Entities.Authority;
import nuiskoffBank.Entities.User;
import nuiskoffBank.Exceptions.TransactionExceptions.WrongCardDataException;

import java.util.List;

public interface UserService {
    public void registerUser(User user);
    public User getUserByUsername(String username);
    public User modifyUserForProfilePage(User user);

    public User getUserByCardNumber(String cardNumber) throws WrongCardDataException;
    public void saveUser(User user);

    public void addRoleToUser(String username,String role);
    public List<Authority> getUserAuthorities(String username);
}
