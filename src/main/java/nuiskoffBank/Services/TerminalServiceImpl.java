package nuiskoffBank.Services;

import nuiskoffBank.Entities.Terminal;
import nuiskoffBank.Entities.User;
import nuiskoffBank.Enums.TerminalCategories;
import nuiskoffBank.Repositories.TerminalRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class TerminalServiceImpl implements TerminalService {
    @Autowired
    private TerminalRepository terminalRepo;

    @Autowired
    private MailNotificationService mailNotificationService;

    @Autowired
    private UserService userService;

    private Logger LOG = LoggerFactory.getLogger(TerminalServiceImpl.class);

    @Override
    @Transactional
    public List<Terminal> getAllTerminals() {
        return terminalRepo.getAllTerminals();
    }

    @Override
    @Transactional
    public Terminal getTerminalByToken(String token) {
        return terminalRepo.getTerminalByToken(token);
    }

    @Override
    @Transactional
    public Terminal createTerminal(String owner,String category,String name, Long telegramID) {
        LOG.info("creating terminal with owner: %s, category: %s, name: %s, telegram id of owner: %d".formatted(owner,category,name,telegramID));

        if (terminalRepo.getTerminalByName(name) != null) {
            LOG.error("failed to create new terminal: terminal with name " + name + " already exists");
            return null;
        }

        Terminal terminal = new Terminal();
        terminal.setOwner(owner);
        terminal.setOwnerID(telegramID);
        terminal.setCategory(category);
        terminal.setName(name);

        terminal.setToken(UUID.randomUUID().toString());
        LOG.debug("generated terminal object: " + terminal);
        terminalRepo.createTerminal(terminal);

        User ownerData = userService.getUserByUsername(terminal.getOwner());
        String body = """
                Перейдите по ссылке для подтверждения создания терминала
                %s
                
                Если вы не запрашивали терминал просто проигнорируйте сообщение"""
                .formatted("localhost:8080/confirm-terminal/" + terminal.getToken());
        mailNotificationService.sendMail(ownerData.getEmail(),"Подтверждение терминала",body);
        return terminal;
    }

    @Override
    @Transactional
    public List<Terminal> getTerminalByOwnerTelegramID(Long id) {
        return terminalRepo.getTerminalByOwnerTelegramID(id);
    }

    @Override
    @Transactional
    public Terminal confirmTerminal(String token) {
        Terminal terminal = terminalRepo.getTerminalByToken(token);
        if (terminal.isActive()) {
            LOG.debug("tried to confirm active terminal");
            return terminal;
        }

        terminal = terminalRepo.confirmTerminal(token);
        LOG.info("terminal " + terminal.getToken() + " confirmed");
        return terminal;
    }

    @Override
    public List<Terminal> getTerminalsByUsername(String username) {
        return terminalRepo.getTerminalsByUsername(username);
    }

    @Override
    @Transactional
    public Terminal getTerminalByName(String name) {
        return terminalRepo.getTerminalByName(name);
    }
}
