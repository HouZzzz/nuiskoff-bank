package nuiskoffBank.Services;

import nuiskoffBank.Entities.TechSupportFeedback;
import nuiskoffBank.Entities.TechSupportQuestion;
import nuiskoffBank.Repositories.FeedbackRepository;
import nuiskoffBank.Repositories.TechSupportQuestionRepository;
import nuiskoffBank.TelegramBot.TechSupportTgBot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FeedbackServiceImpl implements FeedbackService {
    @Autowired
    private FeedbackRepository feedbackRepo;
    @Autowired
    private TechSupportQuestionRepository tsqRepo;

    private static Logger LOG = LoggerFactory.getLogger(FeedbackService.class);

    @Override
    @Transactional
    public void saveFeedback(TechSupportFeedback feedback) {
        TechSupportQuestion lastUserQuestion = tsqRepo.getLastUserQuestion(feedback.getFrom());
        feedback.setTo(lastUserQuestion.getTakenBy());

        feedbackRepo.saveFeedback(feedback);
    }

    @Override
    @Transactional
    public List<TechSupportFeedback> getFeedbacksAboutEmployee(String employeeUsername) {
        return feedbackRepo.getFeedbacksAboutEmployee(employeeUsername);
    }
}
