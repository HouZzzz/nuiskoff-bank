package nuiskoffBank.Services;

import nuiskoffBank.Entities.TechSupportFeedback;

import java.util.List;

public interface FeedbackService {
    public void saveFeedback(TechSupportFeedback feedback);
    public List<TechSupportFeedback> getFeedbacksAboutEmployee(String employeeUsername);
}
