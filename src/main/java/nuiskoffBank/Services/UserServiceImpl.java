package nuiskoffBank.Services;

import nuiskoffBank.Entities.Authority;
import nuiskoffBank.Entities.User;
import nuiskoffBank.Exceptions.TransactionExceptions.WrongCardDataException;
import nuiskoffBank.Repositories.UserRepository;
import nuiskoffBank.Repositories.UserRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepo;
    private Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    @Transactional
    public void registerUser(User user) {
        if (getUserByUsername(user.getUsername()) != null) {
            LOG.debug("user with username %s is already registered".formatted(user.getUsername()));
            return;
        }
        user.setPassword("{noop}" + user.getPassword());
        user.setName(
                user.getName().toUpperCase().substring(0,1) + user.getName().toLowerCase().substring(1,user.getName().length())
        );
        user.setSurname(
                user.getSurname().toUpperCase().substring(0,1) + user.getSurname().toLowerCase().substring(1,user.getSurname().length())
        );
        LOG.debug("corrected user data (password, name, surname)");

        Random random = new Random();

        String generatedCardNumber = "";
        for (int i = 1; i <= 16; i++) {
            generatedCardNumber += random.nextInt(10);
        }
        user.setCardNumber(generatedCardNumber);

        String generatedCvc = "";
        for (int i = 0; i < 3; i++) {
            generatedCvc += random.nextInt(10);
        }
        user.setCvc(generatedCvc);

        Date date = new Date();
        String generatedTerm = (date.getMonth() + 1) + "/" + (date.getYear() - 94);
        user.setTerm(generatedTerm);
        LOG.debug("user card data generated");

        LOG.debug("corrected user with generated card: " + user);

        userRepo.registerUser(user);
    }

    @Override
    @Transactional
    public User getUserByUsername(String username) {
        return userRepo.getUserByUsername(username);
    }

    @Override
    public User modifyUserForProfilePage(User user) {
        LOG.debug("modifying user for profile page");
        String splitedCardNumber = "";
        int i = 1;
        for (String n : user.getCardNumber().split("")) {
            splitedCardNumber += (i % 4 == 0) ? n + " " : n;
            i++;
        }
        user.setCardNumber(splitedCardNumber);
        return user;
    }

    @Override
    @Transactional
    public User getUserByCardNumber(String cardNumber) throws WrongCardDataException {
        LOG.debug("finding user with card number " + cardNumber);
        User user = userRepo.getUserByCardNumber(cardNumber.replaceAll(" ", ""));

        if (user == null) {
            throw new WrongCardDataException("cant find user with card number " + cardNumber);
        } else {
            return user;
        }
    }

    @Override
    @Transactional
    public void saveUser(User user) {
        userRepo.saveUser(user);
    }

    @Override
    @Transactional
    public void addRoleToUser(String username,String role) {
        userRepo.addRoleToUser(username,role);
    }

    @Override
    @Transactional
    public List<Authority> getUserAuthorities(String username) {
        return userRepo.getUserAuthorities(username);
    }
}
