package nuiskoffBank.Configs;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import nuiskoffBank.TelegramBot.TechSupportTgBot;
import nuiskoffBank.TelegramBot.TgBot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;

@Configuration
@ComponentScan("nuiskoffBank")
@EnableWebMvc
@EnableTransactionManagement
public class Config {
    private Logger LOG = LoggerFactory.getLogger(Config.class);
    @Bean
    public DataSource dataSource() {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        try {
            dataSource.setDriverClass("com.mysql.cj.jdbc.Driver");
            dataSource.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/nuiskoffbank?useSSL=false&serverTimezone=UTC");
            dataSource.setUser("root");
            dataSource.setPassword("root");

            LOG.debug("data source bean created");
            return dataSource;
        } catch (PropertyVetoException e) {
            LOG.info("Ошибка при подключении к базе данных");
            throw new RuntimeException(e);
        }
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("nuiskoffBank.Entities");

        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        hibernateProperties.setProperty("hibernate.show_sql","false");

        sessionFactory.setHibernateProperties(hibernateProperties);
        LOG.debug("session factory bean created");
        return sessionFactory;
    }

    @Bean
    public TgBot bot() throws TelegramApiException {
        TgBot bot = new TgBot(new DefaultBotOptions());
        TelegramBotsApi api = new TelegramBotsApi(DefaultBotSession.class);
        api.registerBot(bot);

        LOG.info("bot created");
        return bot;
    }

    @Bean
    public TechSupportTgBot TSbot() throws TelegramApiException {
        TechSupportTgBot bot = new TechSupportTgBot(new DefaultBotOptions());
        TelegramBotsApi api = new TelegramBotsApi(DefaultBotSession.class);
        api.registerBot(bot);

        LOG.info("tech support bot created");
        return bot;
    }
}
