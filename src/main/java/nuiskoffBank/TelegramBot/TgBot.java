package nuiskoffBank.TelegramBot;

import nuiskoffBank.Entities.Wrappers.MessageWrapper;
import nuiskoffBank.Services.MailNotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

public class TgBot extends TelegramLongPollingBot {
    @Autowired
    private CommandHandler commandHandler;

    private static Logger LOG = LoggerFactory.getLogger(TgBot.class);

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();

            long startTime = System.nanoTime();
            List<MessageWrapper> messages = commandHandler.handleCommand(message);
            long endTime = System.nanoTime();

            LOG.trace("answer created by " + ((endTime - startTime) / 1_000_000 ) + "ms");
            for (MessageWrapper messageObj : messages) {
                LOG.trace("created text, sending message to @" + message.getFrom().getUserName() + " with text: " + messageObj.getMessage().trim());
                try {
                    execute(SendMessage.builder()
                            .chatId(message.getChatId())
                            .text(messageObj.getMessage())
                            .replyMarkup(messageObj.getMarkups())
                            .parseMode(ParseMode.MARKDOWN)
                            .build());
                } catch (TelegramApiException e) {
                    LOG.error(e.getMessage());
                }
                LOG.debug("message sent");
            }
        } else if (update.hasCallbackQuery()) {
            LOG.debug("got callback query: " + update.getCallbackQuery().getData());
            Message message = update.getCallbackQuery().getMessage();
            message.setText(update.getCallbackQuery().getData());
            message.setFrom(update.getCallbackQuery().getFrom());

            long startTime = System.nanoTime();
            List<MessageWrapper> messages = commandHandler.handleCallback(message);
            long endTime = System.nanoTime();
            LOG.trace("answer created by " + ((endTime - startTime) / 1_000_000) + "ms");
            try {
                for (MessageWrapper messageObj : messages) {
                    LOG.trace("created text, sending message to @" + update.getCallbackQuery().getMessage().getFrom().getUserName() + " with text: " + messageObj.getMessage().trim());
                    execute(SendMessage.builder()
                            .chatId(update.getCallbackQuery().getMessage().getChatId())
                            .text(messageObj.getMessage())
                            .replyMarkup(messageObj.getMarkups())
                            .parseMode(ParseMode.MARKDOWN)
                            .build());
                    LOG.debug("message sent");
                }
            } catch (TelegramApiException e) {
                LOG.error(e.getMessage());
            }
        } else if (update.hasPollAnswer()) {
            LOG.debug("got answer on feedback poll by @" + update.getPollAnswer().getUser().getUserName() + ". Chosen option: " + (update.getPollAnswer().getOptionIds().get(0) + 1));
        }
    }


    @Override
    public String getBotUsername() {
        return "@nuiskoffbank_bot";
    }

    public TgBot(DefaultBotOptions options) {
        super(options);
    }

    @Override
    public String getBotToken() {
        return "5645449501:AAEfqc9K7H4LSZNWL-tfSTwHDutdGfhaalE";
    }

    public void sendMessage(MessageWrapper message, Long chatID) throws TelegramApiException {
        LOG.debug("sending message to " + chatID + " with text: " + message.getMessage());
        execute(SendMessage.builder()
                .chatId(chatID)
                .text(message.getMessage())
                .replyMarkup(message.getMarkups())
                .parseMode(ParseMode.MARKDOWN)
                .build());
    }
}
