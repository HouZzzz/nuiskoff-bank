package nuiskoffBank.TelegramBot;

import nuiskoffBank.Entities.Terminal;
import nuiskoffBank.Entities.User;
import nuiskoffBank.Entities.Wrappers.MessageWrapper;
import nuiskoffBank.Enums.TerminalCategories;
import nuiskoffBank.Services.TerminalService;
import nuiskoffBank.Services.TransactionService;
import nuiskoffBank.Services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CommandHandler {
    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private UserService userService;

    private static Logger LOG = LoggerFactory.getLogger(CommandHandler.class);

    protected List<MessageWrapper> handleCommand(Message message) {
        LOG.info("handling message with text \"" + message.getText() + "\"");
        switch (message.getText().split(" ")[0]) {
            case "/terminal" -> {
                return terminalCommand(message);
            }
            case "/start" -> {
                return startCommand(message);
            }
            case "/documentation" -> {
                return documentationCommand(message);
            }
            case "/help" -> {
                return helpCommand(message);
            }
        }
        List<MessageWrapper> messages = new ArrayList<>();
        messages.add(new MessageWrapper(null,"invalid message"));
        return messages;
    }

    private InlineKeyboardMarkup markupMaker(String text,String data) {
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> buttons = new ArrayList<>();
        List<InlineKeyboardButton> firstRow = new ArrayList<>();

        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setCallbackData(data );
        button.setText(text);

        firstRow.add(button);
        buttons.add(firstRow);

        markup.setKeyboard(buttons);
        return markup;
    }

    protected List<MessageWrapper> handleCallback(Message message) {
        LOG.debug("handling callback: " + message.getText());
        switch (message.getText()) {
            case "myTerminals" -> {
                message.setText("/terminal");
                return handleCommand(message);
            }

            case "howToCreateTerminal" -> {
                String text = "Для создания терминала напишите команду /terminal в формате \n`/terminal {ваш логин на сайте NuiskoffBank} {категория ваших услуг} {название компании}`\n\n" +
                        "Пример: `/terminal admin shop Двоечка`";
                List<MessageWrapper> messages = new ArrayList<>();
                messages.add(new MessageWrapper(null,text));
                messages.add(new MessageWrapper(null,getCategoriesList().toString()));
                return  messages;
            }

            case "documentation" -> {
                message.setText("/documentation");
                return handleCommand(message);
            }
            case "getCategories" -> {
                return List.of(new MessageWrapper(null,getCategoriesList().toString()));
            }
        }
        List<MessageWrapper> messages = new ArrayList<>();
        messages.add(new MessageWrapper(null,"*Упс, на сервере произошла ошибка. попробуйте повторить попытку позже или обратитесь в* [тех. поддержку](t.me/nuiskoffSupport_bot)"));
        return messages;
    }

    private List<MessageWrapper> terminalCommand(Message message) {
        // all user's terminals
        if (message.getText().split(" ").length == 1) {
            LOG.debug("finding terminal with owner telegram ID: " + message.getFrom().getId());
            List<Terminal> terminals = terminalService.getTerminalByOwnerTelegramID(message.getFrom().getId());
            if (terminals == null) {
                String messageToSend = """
                                У вас еще нету терминалов.
                                Для создания терминала отправьте команду в формате /terminal {имя пользователя вашего аккаунта на сайте NuiskoffBank} {категория терминала} {Название вашей организации}
                                пример: /terminal admin market Пятерочка
                                """;
                List<MessageWrapper> messages = new ArrayList<>();
                messages.add(new MessageWrapper(null,messageToSend));
                return messages;
            } else {
                List<MessageWrapper> messages = new ArrayList<>();

                terminals = terminals.stream()
                        .filter(t -> t.isActive())
                        .sorted((t2,t1) -> t1.getBalance() - t2.getBalance())
                        .collect(Collectors.toList());

                for (Terminal terminal : terminals) {
                    String messageToSend = """
                                Ваш терминал *%s*:
                                    Токен терминала: `%s`
                                    Баланс терминала: %d
                                    Количество транзакций: %d
                                """.formatted(
                            terminal.getName(),
                            terminal.getToken(),
                            terminal.getBalance(),
                            transactionService.getTransactionsByTerminalToken(terminal.getToken()).size());
                    messages.add(new MessageWrapper(null,messageToSend));
                }
                return messages;
            }

        }

        // вывод информации о терминале
        if (message.getText().split(" ").length == 2) {
            String terminalName= message.getText().split(" ")[1];
            LOG.debug("getting info by terminal " + terminalName);

            Terminal terminal = terminalService.getTerminalByName(terminalName);

            if (terminal == null || !terminal.getOwnerID().equals(message.getChatId())) {
                LOG.debug("user %s is not owner of terminal %s or this terminal does not exist".formatted(message.getFrom().getUserName(),terminalName));
                return List.of(new MessageWrapper(markupMaker("Как создать терминал❓", "howToCreateTerminal"),"У вас нету терминала с названием " + terminalName));
            }

            int transactionsCounter = transactionService.getTransactionsByTerminalToken(terminal.getToken()).size();

            String messageToSend = """
                                Ваш терминал *%s*:
                                    Токен терминала: `%s`
                                    Баланс терминала: %d
                                    Количество транзакций: %d
                                """.formatted(
                                        terminalName,
                                        terminal.getToken(),
                                        terminal.getBalance(),
                                        transactionsCounter);


            return List.of(new MessageWrapper(null,messageToSend));
        }

        // создание терминала
        if (true) { // создаем отдельный скоуп для переменных
            LOG.debug("creating terminal by command: " + message.getText());
            String[] s = message.getText().split(" ");

            boolean isCategoryAllowed = false;
            String category = s[2];

            // checking for category id
            try {
                int categoryID = Integer.parseInt(category);

                TerminalCategories[] categories = TerminalCategories.values();
                if (categoryID > categories.length) {
                    LOG.error("Cant find category with id " + categoryID);

                    List<MessageWrapper> messageWrapper = new ArrayList<>();
                    messageWrapper.add(new MessageWrapper(MarkupMaker.markupMaker("Доступные категории", "getCategories"),
                            "Недоступная категория терминала"));

                    return messageWrapper;
                }
                category = categories[categoryID - 1].getCategory();

                isCategoryAllowed = true;
            } catch (NumberFormatException e) {
                LOG.error("fail during parse category ID to integer: " + e.getMessage());

                // checking is category allow
                for (TerminalCategories enumCategory : TerminalCategories.values()) {
                    if (enumCategory.getCategory().equals(category)) {
                        isCategoryAllowed = true;
                        break;
                    }
                }
            }

            if (!isCategoryAllowed) {
                LOG.error("error during creating terminal: not allowed category");
                return List.of(new MessageWrapper(MarkupMaker.markupMaker("Доступные категории","getCategories"),
                        "Недоступная категория терминала"));
            }

            Terminal createdTerminal = terminalService.createTerminal(s[1], category, s[3],message.getFrom().getId());

            if (createdTerminal == null) {
                return List.of(new MessageWrapper(null,"Терминал с названием " + s[3] + " уже существует, выберите другое название или обратитесь в [тех. поддержку](t.me/nuiskoffSupport_bot)"));
            }

            User owner = userService.getUserByUsername(s[1]);
            String messageToSend = "На почту %s отправлено сообщение. Перейдите по ссылке в сообщении для активации терминала.".formatted(owner.getEmail());

            List<MessageWrapper> messages = new ArrayList<>();
            messages.add(new MessageWrapper(markupMaker("Документация\uD83D\uDDD2", "documentation"),messageToSend));
            return messages;
        }
        return null;
    }

    private List<MessageWrapper> startCommand(Message message) {
        String startMessage = """
                       Привет! В этом боте ты сможешь зарегистрировать терминал на свой банковский аккаунт и использовать его в своих целях.
                        """;

        List<MessageWrapper> msg = new ArrayList<>();
        msg.add(new MessageWrapper(markupMaker("Как создать терминал❓", "howToCreateTerminal"),startMessage));
        return msg;
    }

    private List<MessageWrapper> helpCommand(Message message) {
        String terminalHelpText = """
                `/terminal`
                    /terminal: вывод информации о всех ваших терминалах.
                    /terminal {название терминала}: информация о терминале с данным названием (только если Вы являетесь владельцем).
                    /terminal {логин на сайте} {категория} {название}: регистрация терминала. для подробной информации нажмите на кнопку под сообщением.
                    
                """;
        String documentationHelpText = "`/documentation`\n\tДокументация по использованию Nuiskoff Bank API";

        return List.of(
                new MessageWrapper(markupMaker("Как создать терминал❓", "howToCreateTerminal"),terminalHelpText),
                new MessageWrapper(markupMaker("Документация\uD83D\uDDD2", "documentation"),documentationHelpText));
    }

    private List<MessageWrapper> documentationCommand(Message message) {
        List<MessageWrapper> messages = new ArrayList<>();

        String getTerminalInfo = "Получение информации о терминале: \nGET | `localhost:8080/nuiskoffBank/api/terminals/{token}`\n" +
                "Пример: `localhost:8080/nuiskoffBank/api/terminals/09e91d6f-7a91-4e8c-b052-47f16b426a57` будет выглядеть так:\n" +
                "*{\n" +
                "    \"id\": 5,\n" +
                "    \"name\": \"Двоечка\",\n" +
                "    \"token\": \"09e91d6f-7a91-4e8c-b052-47f16b426a57\",\n" +
                "    \"balance\": 0,\n" +
                "    \"category\": \"shop\",\n" +
                "    \"owner\": \"houzz\",\n" +
                "    \"ownerID\": 1016872254\n" +
                "}*";
        messages.add(new MessageWrapper(null,getTerminalInfo));

        String getTransactions = "Получение последних 10 транзакций терминала:\nGET | `localhost:8080/nuiskoffBank/api/terminals/transactions/{token}`" +
                "Пример: `localhost:8080/nuiskoffBank/api/terminals/transactions/38e8047b-ae54-4cb5-a1fa-7437bb72420b` будет выглядеть так:\n" +
                "*[\n" +
                "    {\n" +
                "        \"id\": 2,\n" +
                "        \"sender\": \"houzz\",\n" +
                "        \"getter\": null,\n" +
                "        \"getterTerminalToken\": \"38e8047b-ae54-4cb5-a1fa-7437bb72420b\",\n" +
                "        \"amount\": 599,\n" +
                "        \"date\": 1670859488000,\n" +
                "        \"confirmCode\": 1111,\n" +
                "        \"confirmed\": true,\n" +
                "        \"message\": null\n" +
                "    }\n" +
                "    {...}\n" +
                "]*";
        messages.add(new MessageWrapper(null,getTransactions));

        String createTransaction = "Создание транзакции:\nPOST | `localhost:8080/nuiskoffBank/api/terminals/transactions`\n" +
                "Тело запроса должно быть в формате:\n" +
                "*{\n" +
                "    \"senderCardNumber\" : \"{card number}\",\n" +
                "    \"senderTerm\" : \"{card term}\",\n" +
                "    \"senderCvc\" : \"{card cvc}\",\n" +
                "    \"terminalToken\" : \"{token}\",\n" +
                "    \"amount\" : {amount}\n" +
                "}*\n\n" +
                "Пример:\n" +
                "*{\n" +
                "    \"senderCardNumber\" : \"3376801662040312\",\n" +
                "    \"senderTerm\" : \"03/24\",\n" +
                "    \"senderCvc\" : \"591\",\n" +
                "    \"terminalToken\" : \"38e8047b-ae54-4cb5-a1fa-7437bb72420b\",\n" +
                "    \"amount\" : 599\n" +
                "}*";
        messages.add(new MessageWrapper(markupMaker("Мои терминалы", "myTerminals"),createTransaction));
        return messages;
    }

    private StringBuilder getCategoriesList() {
        StringBuilder categories = new StringBuilder("Выберите одну из доступных категорий:\n\n");

        int index = 1;
        for (TerminalCategories category : TerminalCategories.values()) {
            categories.append(index);
            categories.append(") `" + category.getCategory() + "`\n");
            index++;
        }

        categories.append("\n_Напишите название категории или его порядковый номер_");
        return categories;
    }
}
