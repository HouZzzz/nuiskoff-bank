package nuiskoffBank.TelegramBot;

import nuiskoffBank.Entities.Wrappers.MessageWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.polls.SendPoll;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.polls.PollAnswer;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.*;
import java.util.stream.Collectors;

public class TechSupportTgBot extends TelegramLongPollingBot {

    @Autowired
    TechSupportBotHandler handler;

    private static Logger LOG = LoggerFactory.getLogger(TechSupportTgBot.class);

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();
            List<MessageWrapper> messages =
                    null;
            try {
                messages = message.isCommand() ? handler.handleCommand(message) : handler.handleMessage(message);
            } catch (TelegramApiException e) {
                throw new RuntimeException(e);
            }

            for (MessageWrapper messageObj : messages) {
                LOG.trace("created text, sending message to @" + (messageObj.getChatID() == 0 ? message.getFrom().getUserName() : messageObj.getChatID()) + " with text: " + messageObj.getMessage().trim());
                try {
                    execute(SendMessage.builder()
                            .chatId(messageObj.getChatID() == 0 ? message.getChatId() : messageObj.getChatID())
                            .text(messageObj.getMessage())
                            .replyMarkup(messageObj.getMarkups())
                            .parseMode(ParseMode.MARKDOWN)
                            .build());
                } catch (TelegramApiException e) {
                    LOG.error(e.getMessage());
                }
                LOG.debug("message sent");
            }
        }
        if (update.hasCallbackQuery()) {
            CallbackQuery callbackQuery = update.getCallbackQuery();
            if (callbackQuery.getData().equals("nullQuery")) {
                return;
            }
            List<MessageWrapper> messages = handler.handleCallback(callbackQuery);

            for (MessageWrapper newMessage : messages) {
                if (newMessage.isPoll()) {
                    SendPoll poll = new SendPoll();
                    poll.setChatId(newMessage.getChatID() == 0 ? callbackQuery.getMessage().getChatId() : newMessage.getChatID());
                    poll.setOptions(newMessage.getOptions());
                    poll.setQuestion(newMessage.getMessage());
                    poll.setIsAnonymous(false);
                    poll.setReplyMarkup(newMessage.getMarkups());
                    poll.setAllowMultipleAnswers(false);
                    // todo засчитывать оценки ответчику
                    try {
                        execute(poll);
                    } catch (TelegramApiException e) {
                        LOG.error(e.getMessage());
                    }
                    return;
                } else {
                    try {
                        execute(EditMessageText.builder()
                                .chatId(callbackQuery.getMessage().getChatId())
                                .messageId(Math.toIntExact(callbackQuery.getMessage().getMessageId()))
                                .text(newMessage.getMessage())
                                .replyMarkup(newMessage.getMarkups())
                                .parseMode(ParseMode.MARKDOWN)
                                .build());
                    } catch (TelegramApiException e) {
                        LOG.error(e.getMessage());
                    }
                }
            }
        }
        if (update.hasPollAnswer()) {
            PollAnswer pollAnswer = update.getPollAnswer();
            handler.handlePollAnswer(pollAnswer);
        }
    }

    @Override
    public String getBotUsername() {
        return "@nuiskoffSupport_bot";
    }

    public TechSupportTgBot(DefaultBotOptions options) {
        super(options);
    }

    @Override
    public String getBotToken() {
        return "5730908067:AAGki_ubtUoEJGS5fltIsen0UuxrIM8xLps";
    }
}
