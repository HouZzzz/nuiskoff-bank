package nuiskoffBank.TelegramBot;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

public class MarkupMaker {
    public static InlineKeyboardMarkup markupMaker(String text, String data) {
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> buttons = new ArrayList<>();
        List<InlineKeyboardButton> firstRow = new ArrayList<>();

        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setCallbackData(data);
        button.setText(text);

        firstRow.add(button);
        buttons.add(firstRow);

        markup.setKeyboard(buttons);
        return markup;
    }
    public static InlineKeyboardMarkup markupMaker(List<String> texts,List<String> dates) {
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> buttons = new ArrayList<>();
        List<InlineKeyboardButton> firstRow = new ArrayList<>();

        for (int i = 0; i < texts.size(); i++) {
            InlineKeyboardButton button = new InlineKeyboardButton();

            button.setCallbackData(dates.get(i));
            button.setText(texts.get(i));

            firstRow.add(button);
        }
        buttons.add(firstRow);

        markup.setKeyboard(buttons);
        return markup;
    }
}
