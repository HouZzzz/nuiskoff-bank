package nuiskoffBank.TelegramBot;

import nuiskoffBank.Entities.TechSupportFeedback;
import nuiskoffBank.Entities.TechSupportQuestion;
import nuiskoffBank.Entities.Wrappers.MessageWrapper;
import nuiskoffBank.Enums.QuestionStatuses;
import nuiskoffBank.Services.FeedbackService;
import nuiskoffBank.Services.TechSupportQuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.polls.PollAnswer;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
public class TechSupportBotHandler {

    private List<String> techSupportAdmins = new ArrayList<>();
    @Autowired
    private TechSupportQuestionService tsqService;

    @Autowired
    private FeedbackService feedbackService;

    private static Logger LOG = LoggerFactory.getLogger(TechSupportBotHandler.class);
    protected List<MessageWrapper> handleCommand(Message message) {
        LOG.debug("handling command " + message.getText());
        switch (message.getText().split(" ")[0]) {
            case "/start" -> {
                return List.of(new MessageWrapper(null,
                        "Если у вас возникли проблемы с использованием наших сервисов опишите свою проблему в этом чате и мы ответим вам как можно скорее!"));
            }
            case "/admin" -> {
                return adminCommand(message);
            }
        }
        return List.of(new MessageWrapper(null, "Неизвестная команда"));
    }


    protected List<MessageWrapper> handleCallback(CallbackQuery callback) {
        String data = callback.getData();
        LOG.debug("handling callback with data: " + data);

        switch (data) {
            case "checkQuestionStatus" -> {
                if (!techSupportAdmins.contains(callback.getFrom().getUserName())) {
                    return List.of(new MessageWrapper(null,"Доступ к командам работников запрещен"));
                }
                int questionID = Integer.parseInt(callback.getMessage().getText().split("\n")[0]);
                return List.of(checkQuestionStatus(questionID,callback.getMessage().getText()));
            }
            case "doRequest" -> {
                if (!techSupportAdmins.contains(callback.getFrom().getUserName())) {
                    return List.of(new MessageWrapper(null,"Доступ к командам работников запрещен"));
                }
                int questionID = Integer.parseInt(callback.getMessage().getText().split("\n")[0]);
                return List.of(requestToAnswer(questionID,callback.getMessage().getText(),callback.getFrom().getUserName()));
            }
            case "closeQuestion" -> {
                new Thread(() -> {
                    tsqService.closeQuestion(callback.getMessage().getChatId());
                }).start();
                return List.of(new MessageWrapper(MarkupMaker.markupMaker("✅","nullQuery"),callback.getMessage().getText() + "\n\nВопрос закрыт, оцените качество работы технической поддержки."),
                                new MessageWrapper(null,"Оцените работу технической поддержки",true,List.of("5","4","3","2","1")));
            }
        }
        return List.of(new MessageWrapper(callback.getMessage().getReplyMarkup(),callback.getMessage().getText()));
    }

    protected List<MessageWrapper> handleMessage(Message message) throws TelegramApiException {
        // answer on question
        if (message.getReplyToMessage() != null && techSupportAdmins.contains(message.getFrom().getUserName())) {
            List<MessageWrapper> messageWrappers = answerToQuestion(
                    message.getReplyToMessage().getText(), // question
                    message.getText(), // answer
                    message.getFrom().getUserName() // tsEmpUsername
            );
            return messageWrappers;
        }
        // question
        tsqService.createQuestion(
                message.getText(),
                message.getFrom().getId()
        );

        return List.of(new MessageWrapper(null,"Ваша проблема отправлена службе поддержки на рассмотрение"));
    }

    protected void handlePollAnswer(PollAnswer pollAnswer) {
        Map<Integer, Integer> optionToFeedback = Map.of(
                0, 5,
                1, 4,
                2, 3,
                3, 2,
                4, 1);

        int feedbackValue = optionToFeedback.get(pollAnswer.getOptionIds().get(0));
        LOG.debug("user @" + pollAnswer.getUser().getUserName() + " gave feedback with value " + feedbackValue);
        feedbackService.saveFeedback(new TechSupportFeedback(
                        String.valueOf(pollAnswer.getUser().getId()),
                        feedbackValue));
    }

    private List<MessageWrapper> adminCommand(Message message) {

        String username = message.getFrom().getUserName();

        // проверка наличия прав админа
        if (techSupportAdmins.contains(username)) {
            String[] splitedCommand = message.getText().split(" ");

            switch (splitedCommand[1]) {
                // открытые вопросы
                case "q" -> {
                    List<TechSupportQuestion> openQuestions = tsqService.getOpenQuestions();
                    List<MessageWrapper> messages = new ArrayList<>();

                    if (openQuestions.size() == 0) {
                        messages.add(new MessageWrapper(null,"Пока что нету открытых вопросов, повторите попытку позже"));
                    } else {
                        openQuestions
                                .forEach(question -> {
                                    messages.add(new MessageWrapper(MarkupMaker.markupMaker(
                                            List.of("Статус вопроса","Ответить на вопрос"),List.of("checkQuestionStatus","doRequest"))
                                            ,question.getId() + "\n" + question.getProblem()));
                                });
                    }
                    return messages;
                }
                case "feedback" -> {
                    List<TechSupportFeedback> feedbacks = feedbackService.getFeedbacksAboutEmployee(
                            message.getFrom().getUserName());

                    String messageText = feedbackMessage(feedbacks).toString();
                    return List.of(new MessageWrapper(null,messageText));
                }
            }
        }
        return List.of(new MessageWrapper(null,"У вас нету прав админа технической поддержки"));
    }

    private MessageWrapper checkQuestionStatus(int questionID,String messageText) {
        messageText = clearPastStatusRequests(messageText);

        TechSupportQuestion question = tsqService.getQuestionById(questionID);

        String status = question.getStatus();
        boolean canAnswer = false;
        if (status.equals(QuestionStatuses.FREE.getStatus())) {
            status = "*Статус: вопрос никем не взят*";
            canAnswer = true;
        } else {
            if (question.isClosed()) {
                status = "*Статус: вопрос уже решен*";
            } else {
                status = "*Статус: вопрос решается другим сотрудником*";
            }
        }
        messageText += "\n\n" + status;

        if (canAnswer) {
            return new MessageWrapper(MarkupMaker.markupMaker(
                    List.of("Статус вопроса","Ответить на вопрос"),List.of("checkQuestionStatus","doRequest")),
                    messageText);
        }
        //else
        return new MessageWrapper(MarkupMaker.markupMaker("Статус вопроса","checkQuestionStatus"),messageText);
    }

    private MessageWrapper requestToAnswer(int questionID,String messageText,String adminUsername) {
        messageText = clearPastStatusRequests(messageText);

        if (!techSupportAdmins.contains(adminUsername)) {
            return new MessageWrapper(null, "Доступ к правам работников тех. поддержки закрыт.");
        }
        LOG.debug(adminUsername + " taking #question" + questionID);
        TechSupportQuestion question = tsqService.requestToAnswer(questionID,adminUsername);
        if (question == null) {
            return new MessageWrapper(MarkupMaker.markupMaker("Статус вопроса", "checkQuestionStatus"),messageText + "\n\n*Статус: Вопрос уже взят или закрыт сотрудником*");
        }

        LOG.debug("#question" + questionID + " successfully taken by " + adminUsername);
        return new MessageWrapper(MarkupMaker.markupMaker("Статус вопроса","checkQuestionStatus"),messageText + "\n\n*Статус: Вопрос взят вами, напишите свой ответ отметив это сообщение*");
    }

    private List<MessageWrapper> answerToQuestion(String questionText,String answer,String tsEmployeeUsername) throws TelegramApiException {
        int questionID = Integer.parseInt(questionText.split("\n")[0]);
        TechSupportQuestion question = tsqService.getQuestionById(questionID);
        Long chatID = Long.parseLong(question.getCreatorTelegramID());

        if (question.isClosed()) {
            return List.of(new MessageWrapper(null,"Вопрос уже закрыт"));
        }
        if (!question.getTakenBy().equals(tsEmployeeUsername)) {
            return List.of(new MessageWrapper(null,"Вы не взяли на себя этот вопрос или его взял другой работник."));
        }

        return List.of(new MessageWrapper(null,"Ответ на вопрос отправлен"),
                        new MessageWrapper(MarkupMaker.markupMaker("Проблема решена, закрыть мой вопрос.","closeQuestion"),"*Ответ сотрудника службы поддержки*\n\n" + answer,chatID));
    }


    private String clearPastStatusRequests(String messageText) {
        String[] splitedMessageText = messageText.split("\n\n");
        if (splitedMessageText[splitedMessageText.length-1].startsWith("Статус:")) {
            messageText = "";
            splitedMessageText[splitedMessageText.length - 1] = "";
            for (String s : splitedMessageText) messageText += s;
        }
        return messageText;
    }
    private StringBuilder feedbackMessage(List<TechSupportFeedback> feedbacks) {
        StringBuilder messageText = new StringBuilder();

        messageText.append("Общее количество оценок вашей работы: " + feedbacks.size() + "\n");

        double average = feedbacks.stream()
                .mapToInt(i -> i.getValue())
                .average().getAsDouble();
        messageText.append("Ваша средняя оценка: %.1f\n\n".formatted(average));

        List<Integer> feedbacksValues = feedbacks.stream()
                .map(i -> i.getValue())
                .collect(Collectors.toList());

        Map<Object,Long> countedValues = feedbacksValues.stream()
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()));

        for (Object fbValue : countedValues.keySet()) {
            messageText.append(fbValue).append(": ").append(countedValues.get(fbValue)).append("\n");
        }
        LOG.debug("created message with feedbacks data: " + messageText.toString());

        return messageText;
    }


    public TechSupportBotHandler() {

        // список юзернеймов пользователей в телеграме с правами администратора тех поддержки
        techSupportAdmins = List.of(
                "H0uZz"
        );
    }
}
