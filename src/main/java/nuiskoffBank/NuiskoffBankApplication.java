package nuiskoffBank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NuiskoffBankApplication {
	public static void main(String[] args) {
		SpringApplication.run(NuiskoffBankApplication.class, args);
	}
}
