package nuiskoffBank.Exceptions.TransactionExceptions;

public class WrongCardDataException extends Exception {
    public WrongCardDataException(String message) {
        super(message);
    }

    public WrongCardDataException() {
        super("Input card data is wrong");
    }

    public WrongCardDataException(Throwable cause) {
        super(cause);
    }

    public WrongCardDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
